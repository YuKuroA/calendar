import "./EventCard.css";
import deleteImage from "../../assetes/delete.jpg";

interface Props {
  title: string;
  marks?: string[];
  onDelete: () => void;
  type: string;
}

export const EventCard: React.FC<Props> = ({
  title,
  marks,
  onDelete,
  type,
}) => {
  const handleDelete = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.stopPropagation();
    onDelete();
  };

  function handleOnclick(event: React.MouseEvent<HTMLDivElement>) {
    event.stopPropagation();
  }

  return (
    <div className="event" onClick={handleOnclick}>
      {marks && marks.length > 0 && (
        <div className="marks">
          {marks.map((mark, index) => (
            <div key={index} className={`mark ${mark.toLowerCase()}`} />
          ))}
        </div>
      )}
      <div className="title">
        {title}
        {type === "event" && (
          <button onClick={handleDelete}>
            <img src={deleteImage} alt="delete icon" className="delete" />
          </button>
        )}
      </div>
    </div>
  );
};
