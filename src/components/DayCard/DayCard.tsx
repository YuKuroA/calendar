import { useState } from "react";
import { EventCard } from "../EventCard/EventCard";
import { EventForm } from "../EventForm/EventForm";
import "./DayCard.css";
import moment from "moment";
import { Draggable, Droppable } from "react-beautiful-dnd";

interface Event {
  title: string;
  marks?: string[];
  date: string;
}

interface Props {
  date: string;
  holidays?: Event[];
  events?: Event[];
  onEventDelete: (title: string) => void;
  onCreateEvent: (newEventData: Event) => void;
}

export const DayCard: React.FC<Props> = ({
  date,
  events,
  holidays,
  onEventDelete,
  onCreateEvent,
}) => {
  const [showForm, setShowForm] = useState(false);

  function handleOnclick() {
    setShowForm(true);
  }

  function onClose() {
    setShowForm(false);
  }

  function handleSubmit(newEventData: Event) {
    onCreateEvent(newEventData);
    onClose();
  }
  const handleDeleteEvent = (title: string) => {
    onEventDelete(title);
  };

  const currentDate = moment(date).date();

  return (
    <div className="day" onClick={handleOnclick}>
      {date && currentDate}
      {date && showForm && (
        <EventForm onClose={onClose} onSubmit={handleSubmit} date={date} />
      )}
      <Droppable droppableId={crypto.randomUUID() + date}>
        {(provided) => (
          <div
            className="event-list"
            ref={provided.innerRef}
            {...provided.droppableProps}
          >
            {events &&
              events.map((event, index) => (
                <Draggable key={index} draggableId={event.title} index={index}>
                  {(provided) => (
                    <div
                      ref={provided.innerRef}
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}
                    >
                      <EventCard
                        key={index}
                        title={event.title}
                        type="event"
                        marks={event.marks}
                        onDelete={() => handleDeleteEvent(event.title)}
                      />
                    </div>
                  )}
                </Draggable>
              ))}
            {provided.placeholder}
            {holidays &&
              holidays.map((holiday, index) => (
                <EventCard
                  key={index}
                  title={holiday.title}
                  type="holiday"
                  marks={holiday.marks}
                  onDelete={() => handleDeleteEvent(holiday.title)}
                />
              ))}
          </div>
        )}
      </Droppable>
    </div>
  );
};
