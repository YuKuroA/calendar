import { useEffect, useState } from "react";
import { DayCard } from "../DayCard/DayCard";
import "./CalendarGrid.css";
import moment from "moment";
import { DragDropContext, DropResult } from "react-beautiful-dnd";

interface Event {
  title: string;
  marks?: string[];
  date: string;
}

interface Props {
  month: number;
  year: number;
}

export const CalendarGrid: React.FC<Props> = ({ month, year }) => {
  const [holidays, setHolidays] = useState<Event[]>([]);
  const [events, setEvents] = useState<Event[]>([]);

  useEffect(() => {
    fetchHolidays();
  }, []);

  const fetchHolidays = async () => {
    try {
      const response = await fetch(
        `https://date.nager.at/api/v3/NextPublicHolidaysWorldwide`
      );
      const data = await response.json();

      const uniqueHolidaysSet = new Set<string>();
      const uniqueHolidays: Event[] = [];

      data.forEach((holiday: any) => {
        if (!uniqueHolidaysSet.has(holiday.name)) {
          uniqueHolidaysSet.add(holiday.name);
          uniqueHolidays.push({
            title: holiday.name,
            date: holiday.date,
            marks: ["green", "yellow"],
          });
        }
      });

      setHolidays(uniqueHolidays);
    } catch (error) {
      console.error("Error fetching holidays:", error);
    }
  };

  const getDaysInMonth = (month: number, year: number) => {
    return new Date(year, month + 1, 0).getDate();
  };
  const firstDayOfMonth = new Date(year, month, 1).getDay();
  const totalDaysInMonth = getDaysInMonth(month, year);
  const startingOffset = firstDayOfMonth === 0 ? 6 : firstDayOfMonth - 1;
  const dates = Array.from(
    { length: totalDaysInMonth + startingOffset },
    (_, index) =>
      index < startingOffset ? undefined : index - startingOffset + 1
  );

  const handleDeleteEvent = (title: string) => {
    setEvents((prevEvents) =>
      prevEvents.filter((event) => event.title !== title)
    );
  };

  const handleCreateEvent = (newEvent: Event) => {
    setEvents((prevEvents) => [...prevEvents, newEvent]);
  };

  const onDragEnd = (result: DropResult) => {
    const { destination, source, draggableId } = result;

    if (!destination) {
      return;
    }

    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    ) {
      return;
    }

    if (destination.droppableId !== source.droppableId) {
      const destinationDate = destination.droppableId.slice(-10);

      const draggedEventIndex = events.findIndex(
        (event) => event.title === draggableId
      );

      if (draggedEventIndex !== -1) {
        const updatedEvents = [...events];
        updatedEvents[draggedEventIndex].date = destinationDate;
        setEvents(updatedEvents);
      }
    } else {
      const newEvents = Array.from(events);
      const [draggedEvent] = newEvents.splice(source.index, 1);
      newEvents.splice(destination.index, 0, draggedEvent);

      setEvents(newEvents);
    }
  };

  return (
    <div className="calendarGrid">
      <div className="weekdays">
        <p>Sun</p>
        <p>Mon</p>
        <p>Tue</p>
        <p>Wed</p>
        <p>Thu</p>
        <p>Fri</p>
        <p>Sat</p>
      </div>
      <DragDropContext onDragEnd={onDragEnd}>
        <div className="month">
          {dates.map((date, index) => {
            const currentDate = !date
              ? ""
              : moment(`${year}-${month + 1}-${date}`).format("YYYY-MM-DD");
            const holidaysForDate = holidays.filter((holiday) =>
              moment(holiday.date).isSame(currentDate)
            );
            const eventsForDate = events.filter((event) =>
              moment(event.date).isSame(currentDate)
            );

            return (
              <DayCard
                key={index}
                date={currentDate}
                events={eventsForDate}
                holidays={holidaysForDate}
                onEventDelete={handleDeleteEvent}
                onCreateEvent={(newEvent) => handleCreateEvent(newEvent)}
              />
            );
          })}
        </div>
      </DragDropContext>
    </div>
  );
};
