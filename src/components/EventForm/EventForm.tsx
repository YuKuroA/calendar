import { useState } from "react";
import "./EventForm.css";

interface Event {
  title: string;
  marks?: string[];
  date: string;
}

interface Props {
  date: string;
  onSubmit: (newEventData: Event) => void;
  onClose: () => void;
}

export const EventForm: React.FC<Props> = ({ onSubmit, onClose, date }) => {
  const [title, setTitle] = useState("");
  const [mark, setMark] = useState("Green");
  const marks = ["Green", "Yellow", "Orange"];

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    const newEventData: Event = {
      title: title,
      marks: [mark],
      date: date,
    };

    onSubmit(newEventData);
    onClose();
  };

  function handleOnClose(event: React.MouseEvent<HTMLButtonElement>) {
    event.stopPropagation();
    onClose();
  }

  return (
    <form className="eventForm" onSubmit={handleSubmit}>
      <input
        type="text"
        className="eventData"
        value={title}
        onChange={(e) => setTitle(e.target.value)}
      />
      <select
        value={mark}
        className="eventData"
        onChange={(e) => setMark(e.target.value)}
      >
        {marks.map((color, index) => (
          <option key={index} value={color}>
            {color}
          </option>
        ))}
      </select>
      <button type="submit">Submit</button>
      <button type="button" onClick={handleOnClose}>
        Close
      </button>
    </form>
  );
};
