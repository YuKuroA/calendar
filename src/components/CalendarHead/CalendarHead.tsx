import "./CalendarHead.css";

interface Props {
  onPrevMonth: () => void;
  onNextMonth: () => void;
  currentMonth: number;
  currentYear: number;
  onDateChange: (year: number, month: number) => void;
}

export const CalendarHead: React.FC<Props> = ({
  onPrevMonth,
  onNextMonth,
  currentMonth,
  currentYear,
  onDateChange,
}) => {
  const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  const handlePrevMonth = () => {
    onPrevMonth();
  };

  const handleNextMonth = () => {
    onNextMonth();
  };

  const handleYearChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const year = parseInt(e.target.value);
    onDateChange(year, currentMonth);
  };

  const handleMonthChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const month = parseInt(e.target.value);
    onDateChange(currentYear, month);
  };

  return (
    <div className="calendarHead">
      <div className="navigation">
        <select
          value={currentMonth}
          onChange={handleMonthChange}
          className="date"
        >
          {monthNames.map((month, index) => (
            <option key={index} value={index}>
              {month}
            </option>
          ))}
        </select>
        <input
          type="number"
          value={currentYear}
          onChange={handleYearChange}
          className="date"
        />
        <button onClick={handlePrevMonth}>▼</button>
        <button onClick={handleNextMonth}>▲</button>
      </div>
      <h1>{`${monthNames[currentMonth]} ${currentYear}`}</h1>
      <div className="toggle">
        <button>Week</button>
        <button className="on">Month</button>
      </div>
    </div>
  );
};
