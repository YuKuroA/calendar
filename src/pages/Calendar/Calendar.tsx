import { useState } from "react";
import { CalendarGrid } from "../../components/CalendarGrid/CalendarGrid";
import { CalendarHead } from "../../components/CalendarHead/CalendarHead";
import "./Calendar.css";

export const Calendar: React.FC = () => {
  const currentDate = new Date();
  const [currentYear, setCurrentYear] = useState(currentDate.getFullYear());
  const [currentMonth, setCurrentMonth] = useState(currentDate.getMonth());

  const nextMonth = () => {
    if (currentMonth === 11) {
      setCurrentMonth(0);
      setCurrentYear(currentYear + 1);
    } else {
      setCurrentMonth(currentMonth + 1);
    }
  };

  const prevMonth = () => {
    if (currentMonth === 0) {
      setCurrentMonth(11);
      setCurrentYear(currentYear - 1);
    } else {
      setCurrentMonth(currentMonth - 1);
    }
  };
  const handleDateChange = (year: number, month: number) => {
    setCurrentYear(year);
    setCurrentMonth(month);
  };
  return (
    <div className="calendarPage">
      <CalendarHead
        onPrevMonth={prevMonth}
        onNextMonth={nextMonth}
        currentMonth={currentMonth}
        currentYear={currentYear}
        onDateChange={handleDateChange}
      />
      <CalendarGrid month={currentMonth} year={currentYear} />
    </div>
  );
};
